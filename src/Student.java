
//TODO Write class Javadoc
public class Student extends Person {
	private long id;
	
	//TODO Write constructor Javadoc
	public Student(String name, long id) {
		super(name); // name is managed by Person
		this.id = id;
	}

	/** return a string representation of this Student. */
	public String toString() {
		return String.format("Student %s (%d)", getName(), id);
	}

	//TODO Write equals
	/**
     * Compare person's by name.
     * They are equal if the name matches.
     * @param other is another Person to compare to this one.
     * @return true if the name is same, false otherwise.
     */
	public boolean equals(Object other) {
		if(other == null) return false;
		if(other.getClass() != this.getClass()) return false;
		Student obj = (Student) other;
		if(this.id==obj.id)
		return true;
		return false;
	}
}
